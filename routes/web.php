<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




// Admin side routes
Route::group(['as'=>'admin.', 'prefix'=> 'admin', 'middleware' => ['auth']], function(){
    // Category
    Route::resource('category', 'Web\CategoryController');

    // Customer
    Route::resource('customer', 'Web\CustomerController');
    Route::get('/customer/edit/{id}', 'Web\CustomerController@edit')->name('customer.edit');

    // Payment
    Route::resource('payment', 'Web\PaymentController');
    Route::get('/payment/edit/{id}', 'Web\PaymentController@edit')->name('payment.edit');

    // Product
    Route::resource('product', 'Web\ProductController');

    // Supplier
    Route::resource('supplier', 'Web\SupplierController');

    // Shipper
    Route::resource('shipper', 'Web\ShipperController');
    Route::get('/shipper/edit/{id}', 'Web\ShipperController@edit')->name('shipper.edit');

    // variant
    Route::resource('variant', 'Web\VariantController');
    Route::get('/variant/edit/{id}', 'Web\VariantController@edit')->name('variant.edit');

    // Variant Options
    Route::resource('voption', 'Web\VariantOptionController');
    Route::get('voption/create', 'Web\VariantOptionController@create')->name('voption.create');
    Route::get('/voption/edit/{id}', 'Web\VariantOptionController@edit')->name('voption.edit');

    //
});







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
