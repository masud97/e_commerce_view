<?php

use App\User;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Shipper;
use App\Sku;
use App\Category;
use App\Customer;
use App\Supplier;
use App\Variant;
use App\VariantOption;
use App\SkuValue;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Category::truncate();
        Supplier::truncate();
        Product::truncate();
        Customer::truncate();
        Shipper::truncate();
        Order::truncate();
        OrderDetail::truncate();
        Sku::truncate();
        Variant::truncate();
        VariantOption::truncate();
        SkuValue::truncate();

        DB::table('category_product')->truncate();
        DB::table('sku_variant')->truncate();


        $userQuantity = 10;
        $categoryQuantity = 30;
        $supplierQuantity = 50;
        $productQuantity = 200;
        $customerQuantity = 100;
        $shipperQuantity = 20;
        $orderQuantity = 100;
        $orderDetailQuantity = 100;
        $skuQuantity = 200;
        $variantQuantity = 5;
        $variantOptionQuantity = 10;
        $skuValueQuantity = 100;

        factory(User::class, $userQuantity)->create();
        factory(Category::class, $categoryQuantity)->create();
        factory(Supplier::class, $supplierQuantity)->create();
        factory(Product::class, $productQuantity)->create()->each(function($product){
            $categories = Category::all()->random(mt_rand(1, 3))->pluck('id'); 

            $product->categories()->attach($categories);
        });
        factory(Customer::class, $customerQuantity)->create();
        factory(Shipper::class, $shipperQuantity)->create();
        factory(Order::class, $orderQuantity)->create();
        factory(OrderDetail::class, $orderDetailQuantity)->create();
        factory(Sku::class, $skuQuantity)->create();
        factory(Variant::class, $variantQuantity)->create()->each(function($variant){
            $skus = Sku::all()->random(mt_rand(1, 3))->pluck('id');
            $variant->skus()->attach($skus); 
        });
        factory(VariantOption::class, $variantOptionQuantity)->create();
        factory(SkuValue::class, $skuValueQuantity)->create();
    }
}
