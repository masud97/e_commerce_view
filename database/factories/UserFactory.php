<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Order;
use App\Product;
use App\Shipper;
use App\Category;
use App\Customer;
use App\Supplier;
use App\OrderDetail;
use App\Variant;
use App\VariantOption;
use App\SkuValue;
use App\Sku;

use Illuminate\Support\Str;
use Faker\Generator as Faker;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$ziMQuWuaQK5iko54HGRLteD9k4XwTHxOCMGhVh2v55GrjgwjFmroe', // password
        'image' => $faker->randomElement(['1.png', '2.png', '3.png', '4.png', '5.png']),
        'remember_token' => Str::random(10),
    ];
});




$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        'company_address' => $faker->address,
        'company_city' => $faker->city,
        'company_phone' => $faker->e164PhoneNumber,
        'company_email' => $faker->unique()->safeEmail,
        'contact_person_name' => $faker->name,
        'contact_person_phone' => $faker->e164PhoneNumber,
        'contact_person_email' => $faker->unique()->safeEmail,
        'payment_status' => $status = $faker->randomElement(['Paid', 'Unpaid']),
        'payment_methods' => $status == 'Unpaid' ? null : $faker->randomElement(['Cash', 'Cheque']),
        'discount' => $faker->numberBetween(0, 10),
        'total_amount' => $faker->numberBetween($min = 1000, $max = 10000),
        'type_of_goods' => $faker->word,
        'goods_description' => $faker->paragraph(1),
    ];
});

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $category = $faker->word,
        'slug' => Str::slug($category),
        'description' => $faker->paragraph(1),
        'status' => $faker->numberBetween(0, 1),
    ];
});


$factory->define(Product::class, function (Faker $faker) {
    return [
        
        'supplier_id' => $faker->numberBetween(1, 50),
        'name' => $faker->word,
        'description' => $faker->paragraph(1),        
        'image' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg']),
    ];
});



$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'user_name' => $faker->unique()->userName,
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->e164PhoneNumber,
        'password' => '$2y$10$ziMQuWuaQK5iko54HGRLteD9k4XwTHxOCMGhVh2v55GrjgwjFmroe',
        'address_1' => $faker->address,
        'address_2' => $faker->address,
        'image' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg']),
    ];
});


$factory->define(Shipper::class, function (Faker $faker) {
    return [
        'company_name' => $faker->company,
        'phone' => $faker->e164PhoneNumber,
    ];
});


$factory->define(Order::class, function (Faker $faker) {
    return [
        'order_number' => $faker->numberBetween(100000, 199999),
        'customer_id' => $faker->numberBetween(1, 100),
        'payment_id' => $faker->numberBetween(1, 5),
        'paid' => $paid = $faker->numberBetween(0, 1),
        'payment_date' => $paid ? $faker->date('Y-m-d') : null,
        'shipping_date' => $faker->dateTimeBetween('+1 day', '+1 week'),
        'required_date' => $faker->dateTimeBetween('+1 day', '+1 week'),
        'shipper_id' => $faker->numberBetween(1, 20),
        'transaction_status' => $faker->randomElement(['delivered', 'onhold']),
        'created_at' => $faker->date('Y-m-d'),
    ];
});


$factory->define(OrderDetail::class, function (Faker $faker) {
    return [
        'product_id' => $faker->numberBetween(1, 200),
        'order_id' => $faker->numberBetween(1, 100),
        'price' => $price = $faker->numberBetween(100, 1000),
        'quantity' => $quantity = $faker->numberBetween(1, 3),
        'discount' => $discount = $faker->numberBetween(0, 10),
        'total' => ($price - (($price * $discount)/100)) * $quantity,
        'size' => $faker->numberBetween(1, 5),
        'color' => $faker->numberBetween(1, 5),
        'created_at' => $faker->date('Y-m-d'),
    ];
});


$factory->define(Sku::class, function (Faker $faker) {
    return [
        'sku' => $faker->postcode,
        'product_id' => $faker->numberBetween(1, 200),
        'quantity' => $quantity = $faker->numberBetween(0, 10),
        'status' => $quantity == 0 ? 'Not available' : 'Available',
        'price' => $faker->numberBetween(100, 10000),
        'discount' => $faker->numberBetween(0, 5),
    ];
});


$factory->define(Variant::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(VariantOption::class, function (Faker $faker) {
    return [
        'variant_id' => $faker->numberBetween(1, 5),
        'name' => $faker->word,
    ];
});


$factory->define(SkuValue::class, function (Faker $faker) {
    return [
        'sku_id' => $faker->numberBetween(0, 200),
        'variant_id' => $faker->numberBetween(0, 5),
        'variant_option_id' => $faker->numberBetween(0, 10),
    ];
});
