<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkuValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sku_values', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sku_id')->index();
            $table->unsignedBigInteger('variant_id')->index();
            $table->unsignedBigInteger('variant_option_id')->index();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('sku_id')->references('id')->on('skus');
            $table->foreign('variant_id')->references('id')->on('variants');
            $table->foreign('variant_option_id')->references('id')->on('variant_options');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sku_values');
    }
}
