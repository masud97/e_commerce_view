<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('company_address')->nullable();
            $table->string('company_city', 100)->nullable();
            $table->string('company_phone', 15)->nullable();
            $table->string('company_email', 100)->nullable();
            $table->string('contact_person_name', 150);
            $table->string('contact_person_phone', 15);
            $table->string('contact_person_email', 100)->nullable();
            $table->string('payment_status', 100)->nullable();
            $table->string('payment_methods', 100)->nullable();
            $table->float('discount')->nullable();
            $table->float('total_amount');
            $table->string('type_of_goods')->nullable();
            $table->string('goods_description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
