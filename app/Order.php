<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\Payment;
use App\Shipper;
use App\OrderDetail;

class Order extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_number',
        'customer_id',
        'payment_id',
        'paid',
        'payment_date',
        'shipping_date',
        'required_date',
        'shipper_id',
        'transaction_status',
        'created_at',
        'updated_at',
    ];

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function payment(){
        return $this->hasOne(Payment::class);
    }

    public function shipper(){
        return $this->hasOne(Shipper::class);
    }

    public function order_detail(){
        return $this->belongsTo(OrderDetail::class);
    }

}
