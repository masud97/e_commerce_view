<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Customer extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_name',
        'mobile',
        'email',
        'password',
        'address_1',
        'address_2',
        'image',
        'created_at',
        'updated_at',
    ];

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
