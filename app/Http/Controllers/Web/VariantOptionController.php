<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\VariantOption;
use App\Variant;
use Brian2694\Toastr\Facades\Toastr;

class VariantOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $variantOptions = VariantOption::latest()->with('variant')->get();

        /*
        echo "<pre>";
        print_r($variantOptions);
        die();
        */
        return view('pages.admin.variant-option.index', ['variantOptions' => $variantOptions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $variants = Variant::all();

        return response()->json(['variants' => $variants], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | unique:variant_options',
            'variant' => 'required',
        ],[
            'name.required' => 'Variant option name is required.',
            'name.unique' => 'This variant option name is already exists.',
            'variant.required' => 'Select variant name'
        ]);

        $data = array(
            'name' => $request->input('name'),
            'variant_id' => $request->input('variant'),
            'create_at' =>date('Y-m-d')
        );

        /*
        echo "<pre>";
        print_r($data);
        die();
        */
        $create = VariantOption::create($data);

        if($create){
            Toastr::success('New variant option has created.', 'success');
            return redirect()->route('admin.voption.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $variants = Variant::all();
        $variantOption = VariantOption::findOrFail($id);

        return response()->json(['variants' => $variants, 'variantOption' => $variantOption], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->input('id');

        $variantOption = VariantOption::findOrFail($id);

        $this->validate($request, [
            'name' => 'required | unique:variant_options,name,' . $variantOption->id,
            'variant' => 'required',
        ],[
            'name.required' => 'Variant option name is required.',
            'name.unique' => 'This variant option name is already exists.',
            'variant.required' => 'Variant name is required'
        ]);        

        $data = array(
            'name' => $request->input('name'),
            'variant_id' => $request->input('variant'),
            'updated_at' => date('Y-m-d')
        );

        $update = VariantOption::where('id', $id)->update($data);

        if($update){
            Toastr::success('Variant option has updated.', 'success');
            return redirect()->route('admin.voption.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
