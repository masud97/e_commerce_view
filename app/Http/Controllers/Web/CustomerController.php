<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::latest()->get();

        return view('pages.admin.customer.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'user_name' => 'required | unique:customers',
            'email' => 'required | unique:customers',
            'mobile' => 'required | unique:customers',
            'password' => 'required | confirmed | min:8',
        ],[
            'name.required' => 'Customer name is required.',
            'user_name.required' => 'Customer username is required',
            'user_name.unique' => 'This username is already exists.',
            'email.required' => 'Email is required',
            'email.unique' => 'This email is already exists.',
            'mobile.required' => 'Mobile number is required',
            'mobile.unique' => 'This mobile number is already exists.',
            'password.required' => 'Password is required.',
            'password.confirmed' => 'Both password are not match.',
            'password.min' => 'Password should be minimum 8 characters long'
        ]);

        $hashPass = Hash::make($request->input('password'));



        $data = array(
            'name' => $request->input('name'),
            'user_name' => $request->input('user_name'),
            'email' => $request->input('email'),
            'mobile' => $request->input('mobile'),
            'password' => $hashPass,
            'address_1' => $request->input('address'),
            'address_2' => $request->input('address_2'),
            'create_at' =>date('Y-m-d')
        );

        /*
        echo "<pre>";
        print_r($data);
        die();
        */
        $create = Customer::create($data);

        if($create){
            Toastr::success('New Customer has created.', 'success');
            return redirect()->route('admin.customer.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        return response()->json(['customer' => $customer], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->input('id');

        $customer = Customer::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'user_name' => 'required | unique:customers,user_name,' . $customer->id,
            'email' => 'required | unique:customers,email,' . $customer->id,
            'mobile' => 'required | unique:customers,mobile,' . $customer->id,
        ],[
            'name.required' => 'Customer name is required.',
            'user_name.required' => 'Customer username is required',
            'user_name.unique' => 'This username is already exists.',
            'email.required' => 'Email is required',
            'email.unique' => 'This email is already exists.',
            'mobile.required' => 'Mobile number is required',
            'mobile.unique' => 'This mobile number is already exists.'
        ]);
       

        $data = array(
            'name' => $request->input('name'),
            'user_name' => $request->input('user_name'),
            'email' => $request->input('email'),
            'mobile' => $request->input('mobile'),
            'address_1' => $request->input('address'),
            'address_2' => $request->input('address_2'),
            'updated_at' =>date('Y-m-d')
        );

        $update = Customer::where('id', $id)->update($data);

        if($update){
            Toastr::success('Customer has updated.', 'success');
            return redirect()->route('admin.customer.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
