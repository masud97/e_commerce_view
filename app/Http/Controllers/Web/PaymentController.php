<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Payment;
use Brian2694\Toastr\Facades\Toastr;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::all();

        return view('pages.admin.payment.index', ['payments' => $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'payment_type' => 'required | unique:payments'
        ],[
            'payment_type.required' => 'Payment type is required.',
            'payment_type.unique' => 'This payment type is already exists.'
        ]);

        $data = array(
            'payment_type' => $request->input('payment_type'),
            'create_at' =>date('Y-m-d')
        );

        /*
        echo "<pre>";
        print_r($data);
        die();
        */
        $create = Payment::create($data);

        if($create){
            Toastr::success('New payment type has created.', 'success');
            return redirect()->route('admin.payment.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = Payment::findOrFail($id);

        return response()->json(['payment' => $payment], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->input('id');

        $payment = Payment::findOrFail($id);

        $this->validate($request, [
            'payment_type' => 'required | unique:payments,payment_type,' . $payment->id,
        ],[
            'payment_type.required' => 'Payment type is required.',
            'payment_type.unique' => 'This payment type is already exists.'
        ]);        

        $data = array(
            'payment_type' => $request->input('payment_type'),
            'updated_at' => date('Y-m-d')
        );

        $update = Payment::where('id', $id)->update($data);

        if($update){
            Toastr::success('Payment has updated.', 'success');
            return redirect()->route('admin.payment.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
