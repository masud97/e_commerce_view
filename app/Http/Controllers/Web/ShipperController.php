<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shipper;
use Brian2694\Toastr\Facades\Toastr;

class ShipperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shippers = Shipper::latest()->get();

        return view('pages.admin.shipper.index', ['shippers' => $shippers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required | unique:shippers',
            'phone' => 'required',
        ],[
            'company_name.required' => 'Company name is required.',
            'company_name.unique' => 'This company name is already exists.',
            'phone.required' => 'Phone number is required.'
        ]);

        $data = array(
            'company_name' => $request->input('company_name'),
            'phone' => $request->input('phone'),
            'create_at' =>date('Y-m-d')
        );

        /*
        echo "<pre>";
        print_r($data);
        die();
        */
        $create = Shipper::create($data);

        if($create){
            Toastr::success('New shipper has created.', 'success');
            return redirect()->route('admin.shipper.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shipper = Shipper::findOrFail($id);

        return response()->json(['shipper' => $shipper], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->input('id');

        $shipper = Shipper::findOrFail($id);

        $this->validate($request, [
            'company_name' => 'required | unique:shippers,company_name,' . $shipper->id,
            'phone' => 'required',
        ],[
            'company_name.required' => 'Company name is required.',
            'company_name.unique' => 'This company name is already exists.',
            'phone.required' => 'phone number is required'
        ]);        

        $data = array(
            'company_name' => $request->input('company_name'),
            'phone' => $request->input('phone'),
            'updated_at' => date('Y-m-d')
        );

        $update = Shipper::where('id', $id)->update($data);

        if($update){
            Toastr::success('Shipper info has updated.', 'success');
            return redirect()->route('admin.shipper.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
