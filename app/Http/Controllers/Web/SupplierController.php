<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Supplier;
use Brian2694\Toastr\Facades\Toastr;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::latest()->get();
        return view('pages.admin.supplier.index', ['suppliers' => $suppliers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'required | unique:suppliers',
            'company_phone' => 'required',
            'contact_person_name' => 'required',
            'contact_person_phone'=> 'required',

        ],[
            'company_name.required' => 'Company name is required.',
            'company_name.unique' => 'This company name is already exists.',
            'company_phone.required' => 'Company phone number is required',
            'contact_person_name.required' => 'Contact person name is required',
            'contact_person_phone.required' => 'Contact person phone is required'
        ]);

        $data = array(
            'company_name' => $request->input('company_name'),
            'company_address' => $request->input('company_address'),
            'company_city' => $request->input('company_city'),
            'company_phone' => $request->input('company_phone'),
            'company_email' => $request->input('company_email'),
            'contact_person_name' => $request->input('contact_person_name'),
            'contact_person_phone' => $request->input('contact_person_phone'),
            'contact_person_email' => $request->input('contact_person_email'),
            'payment_status' => $request->input('payment_status'),
            'payment_methods' => $request->input('payment_methods'),
            'discount' => $request->input('discount'),
            'total_amount' => $request->input('total_amount'),
            'type_of_goods' => $request->input('type_of_goods'),
            'goods_description' => $request->input('goods_description'),
            'created_at' => date('Y-m-d')
        );

        /*
        echo "<pre>";
        print_r($data);
        die();
        */
        $createResult = Supplier::create($data);

        if($createResult){
            Toastr::success('New supplier info has saved.', 'success');
            return redirect()->route('admin.supplier.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('pages.admin.supplier.show', ['supplier' => $supplier]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);
        return view('pages.admin.supplier.edit', ['supplier' => $supplier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name' => 'required | unique:suppliers',
            'company_phone' => 'required',
            'contact_person_name' => 'required',
            'contact_person_phone'=> 'required',

        ],[
            'company_name.required' => 'Company name is required.',
            'company_name.unique' => 'This company name is already exists.',
            'company_phone.required' => 'Company phone number is required',
            'contact_person_name.required' => 'Contact person name is required',
            'contact_person_phone.required' => 'Contact person phone is required'
        ]);

        $data = array(
            'company_name' => $request->input('company_name'),
            'company_address' => $request->input('company_address'),
            'company_city' => $request->input('company_city'),
            'company_phone' => $request->input('company_phone'),
            'company_email' => $request->input('company_email'),
            'contact_person_name' => $request->input('contact_person_name'),
            'contact_person_phone' => $request->input('contact_person_phone'),
            'contact_person_email' => $request->input('contact_person_email'),
            'payment_status' => $request->input('payment_status'),
            'payment_methods' => $request->input('payment_methods'),
            'discount' => $request->input('discount'),
            'total_amount' => $request->input('total_amount'),
            'type_of_goods' => $request->input('type_of_goods'),
            'goods_description' => $request->input('goods_description'),
            'updated_at' => date('Y-m-d')
        );

        $update = Supplier::where('id', $id)->update($data);

        if($update){
            Toastr::success('Supplier info has updated successfully.', 'success');
            return redirect()->route('admin.supplier.show', $id);
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);
        $result = $supplier->delete();

        if($result){
            Toastr::success('Supplier info has deleted.', 'success');
            return redirect()->route('admin.supplier.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }
}
