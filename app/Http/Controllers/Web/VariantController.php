<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Variant;
use Brian2694\Toastr\Facades\Toastr;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $variants = Variant::latest()->get();

        return view('pages.admin.variant.index', ['variants' => $variants]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | unique:variants',
        ],[
            'name.required' => 'Variant name is required.',
            'name.unique' => 'This variant name is already exists.',
        ]);

        $data = array(
            'name' => $request->input('name'),
            'create_at' =>date('Y-m-d')
        );

        $create = Variant::create($data);

        if($create){
            Toastr::success('New variant has saved.', 'success');
            return redirect()->route('admin.variant.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $variant = Variant::findOrFail($id);

        return response()->json(['variant' => $variant], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');

        $variant = Variant::findOrFail($id);

        $this->validate($request, [
            'name' => 'required | unique:variants,name,' . $variant->id,
        ],[
            'name.required' => 'Variant name is required.',
            'name.unique' => 'This variant name is already exists.',
        ]);        

        $data = array(
            'name' => $request->input('name'),
            'updated_at' => date('Y-m-d')
        );

        $update = Variant::where('id', $id)->update($data);

        if($update){
            Toastr::success('Variant name has updated.', 'success');
            return redirect()->route('admin.variant.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
