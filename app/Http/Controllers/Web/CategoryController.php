<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;
use Brian2694\Toastr\Facades\Toastr;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::latest()->get();
        return view('pages.admin.category.index', ['categories' => $categories]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required | unique:categories',
        ],[
            'name.required' => 'Name is required.',
            'name.unique' => 'This category name is already exists.'
        ]);


        $slug = Str::slug($request->input('name'));

        $data = array(
            'name' => $request->input('name'),
            'slug' => $slug,
            'description' => $request->input('description'),
            'created_at' => date('Y-m-d')
        );

        $result = Category::create($data);

        if($result){
            Toastr::success('New Category name saved.', 'success');
            return redirect()->route('admin.category.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $result = $category->delete();

        if($result){
            Toastr::success('Category has deleted.', 'success');
            return redirect()->route('admin.category.index');
        }else{
            Toastr::warning('W00ps! Something went wrong. Try again.', 'warning');
            return redirect()->back();
        }
    }
}
