<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Shipper extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'phone',
        'created_at',
        'updated_at',
    ];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
