<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Product;

class OrderDetail extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'order_id',
        'price_per_unit',
        'quantity',
        'discount',
        'total',
        'size',
        'color',
        'created_at',
        'updated_at',
    ];

    public function order(){
        return $this->hasOne(Order::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
