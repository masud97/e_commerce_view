<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Variant;
use App\SkuValue;

class Sku extends Model
{
    //\\\abstract
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku',
        'quantity',
        'status',
        'price',
        'discount',
        'created_at',
        'updated_at',
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function variants(){
        return $this->belongsToMany(Variant::class);
    }

    public function sku_values(){
        return $this->hasMany(SkuValue::class);
    }
}
