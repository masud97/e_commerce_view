<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sku;
use App\VariantOption;
use App\SkuValue;

class Variant extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
    ];

    public function skus(){
        return $this->belongsToMany(Sku::class);
    }

    public function variant_options(){
        return $this->hasMany(VariantOption::class);
    }

    public function sku_values(){
        return $this->hasMany(SkuValue::class);
    }
}
