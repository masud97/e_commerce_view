<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sku;
use App\Variant;
use App\VariantOption;

class SkuValue extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sku_id',
        'variant_id',
        'variant_option_id',
        'created_at',
        'updated_at',
    ];

    public function sku(){
        return $this->belongsTo(Sku::class);
    }

    public function variant(){
        return $this->belongsTo(Variant::class);
    }

    public function variant_option(){
        return $this->belongsTo(VariantOption::class);
    }
}
