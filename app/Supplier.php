<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Product;

class Supplier extends Model
{
    use SoftDeletes;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'company_address',
        'company_city',
        'company_phone',
        'company_email',
        'contact_person_name',
        'contact_person_phone',
        'contact_person_email',
        'payment_status',
        'payment_methods',
        'discount',
        'total_amount',
        'type_of_goods',
        'goods_description',
        'created_at',
        'updated_at',
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }

}
