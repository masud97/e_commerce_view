<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\OrderDetail;
use App\Sku;
use App\Supplier;

class Product extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id',
        'name',
        'description',
        'image',
        'created_at',
        'updated_at',
    ];

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function order_detail(){
        return $this->belongsTo(OrderDetail::class);
    }

    public function sku(){
        return $this->hasOne(Sku::class);
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }
}
