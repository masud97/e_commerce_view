<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Variant;
use App\SkuValue;

class VariantOption extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'variant_id',
        'name',
        'created_at',
        'updated_at',
    ];

    public function variant(){
        return $this->belongsTo(Variant::class);
    }

    public function sku_values(){
        return $this->hasMany(SkuValue::class);
    }
}
