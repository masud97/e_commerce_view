@extends('layouts.admin')
@section('title', 'Customer')
@push('css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('breathcamp')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{url('/home')}}" >Dashboard</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="{{route('admin.customer.index')}}">Customers</a>
    </li>
</ul>
@endsection
@section('content')
        <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#newCustomer" > Add New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-print"></i> Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                    <tr>
                                        <th style="display:none">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                <span></span>
                                            </label>
                                        </th>
                                        <th> Name </th> 
                                        <th> Username </th> 
                                        <th> Email </th> 
                                        <th> Mobile </th>
                                        <th> Address </th>                                     
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($customers as $customer)
                                        <tr class="odd gradeX"> 
                                            <td style="display:none">
                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                    <input type="checkbox" class="checkboxes" value="1" />
                                                    <span></span>
                                                </label>
                                            </td>                                                                                  
                                            <td> {{ $customer->name }} </td> 
                                            <td> {{ $customer->user_name }} </td>   
                                            <td> {{ $customer->email }} </td> 
                                            <td> {{ $customer->mobile }} </td>
                                            <td>
                                                <b>Address:</b> <address> {{ $customer->address_1 }} </address>
                                                <b>Alternative:</b> <address> {{ $customer->address_2 }} </address>
                                            </td>        
                                            <td style="text-align:center">                                                
                                                <a href="#" onclick="event.preventDefault(); editCustomer({{ $customer->id }});" 
                                                    title="Edit" data-placement="top" data-toggle="tooltip" data-original-title="Edit" class="btn btn-success btn-xs tooltips">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>

                                                <a onclick="event.preventDefault(); deleteCustomer({{ $customer->id }});"
                                                title="Delete Customer" data-placement="top" data-toggle="tooltip" data-original-title="Delete Customer" class="btn btn-danger btn-xs tooltips">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                            <form id="delete-customer-{{ $customer->id }}" action="{{route('admin.customer.destroy',$customer->id)}}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>

<!-- /.modal for new customer -->
<div class="modal fade" id="newCustomer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">New Customer</h4>
            </div>
            <form role="form" action="{{route('admin.customer.store')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Customer Name <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Customer Name" type="text" name="name" required="required"> 
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>


                    <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                        <label>User Name <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-user"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="User Name" type="text" name="user_name" required="required"> 
                            <span class="text-danger">{{ $errors->first('user_name') }}</span>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>Email <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Email" type="text" name="email" required="required">
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>
                    
                    
                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                        <label>Mobile <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-mobile"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Mobile" type="text" name="mobile" required="required"> 
                            <span class="text-danger">{{ $errors->first('mobile') }}</span>                              
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Password <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-key"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Password" type="password" name="password" required="required"> 
                            <span class="text-danger">{{ $errors->first('password') }}</span>                              
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label>Confirm Password <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-key"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Confirm Password" type="password" name="password_confirmation" required="required"> 
                            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>                              
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Address 1</label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Address 1" type="text" name="address">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Address 2</label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Address 2" type="text" name="address_2">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal for new customer -->

<!-- /.modal for edit customer -->
<div class="modal fade" id="editCustomer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Customer</h4>
            </div>
            <form role="form" action="{{route('admin.customer.update',0)}}" method="POST">
                @csrf
                @method('PATCH')
                <div id="idField"></div>
                <div class="modal-body"> 
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Customer Name <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            <div id="nameField"></div>
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                    </div>
        
        
                    <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                        <label>User Name <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-user"></i>
                            </span>
                            <div id="userNameField"></div> 
                            <span class="text-danger">{{ $errors->first('user_name') }}</span>
                        </div>
                    </div>
        
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>Email <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-envelope"></i>
                            </span>
                            <div id="emailField"></div>
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                        <label>Mobile <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-mobile"></i>
                            </span>
                            <div id="mobileField"></div>
                            <span class="text-danger">{{ $errors->first('mobile') }}</span>                              
                        </div>
                    </div>

                    <div class="form-group">
                            <label>Address 1</label>
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <div id="addressField"></div>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label>Address 2</label>
                            <div class="input-group">
                                <span class="input-group-addon input-circle-left">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <div id="address2Field"></div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal for edit customer -->



@endsection

@push('js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
    function editCustomer(id){
        var url = "{{url('/admin/customer/edit')}}/" + id;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            $('#editCustomer').modal('show');
            var $nameField = $('#nameField');
            $nameField.empty();
            $nameField.append('<input class="form-control input-circle-right" placeholder="Name" type="text" name="name" value="'+ data.customer.name +'"> ');
            
            var $userNameField = $('#userNameField');
            $userNameField.empty();
            $userNameField.append('<input class="form-control input-circle-right" placeholder="User Name" type="text" name="user_name" value="'+ data.customer.user_name +'"> ');
            
            var $emailField = $('#emailField');
            $emailField.empty();
            $emailField.append('<input class="form-control input-circle-right" placeholder="Email" type="text" name="email" value="'+ data.customer.email +'"> ');
            
            var $mobileField = $('#mobileField');
            $mobileField.empty();
            $mobileField.append('<input class="form-control input-circle-right" placeholder="Mobile" type="text" name="mobile" value="'+ data.customer.mobile +'"> ');
            
            var $addressField = $('#addressField');
            $addressField.empty();
            $addressField.append('<input class="form-control input-circle-right" placeholder="Address 1" type="text" name="address" value="'+ data.customer.address_1 +'"> ');

            var $address2Field = $('#address2Field');
            $address2Field.empty();
            $address2Field.append('<input class="form-control input-circle-right" placeholder="Address 2" type="text" name="address_2" value="'+ data.customer.address_2 +'"> ');
            

            var $idField = $('#idField');
            $idField.empty();
            $idField.append('<input class="form-control" type="hidden" name="id" value="'+ data.customer.id +'"> ');
        });
    }

    function deleteVeriantOption(id){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {                
                document.getElementById('delete-voption-'+id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your file is safe :)',
                'error'
                )
            }
            })
    }
</script>
@endpush
