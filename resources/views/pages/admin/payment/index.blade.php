@extends('layouts.admin')
@section('title', 'Payments')
@push('css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('breathcamp')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{url('/home')}}" >Dashboard</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="{{route('admin.payment.index')}}">Payments</a>
    </li>
</ul>
@endsection
@section('content')
        <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#newPayment"> Add New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-print"></i> Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                    <tr>
                                        <th style="display:none">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                <span></span>
                                            </label>
                                        </th>
                                        <th> Sl. </th>
                                        <th> Payment Type </th>                                        
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($payments as $key => $payment)
                                        <tr class="odd gradeX"> 
                                            <td style="display:none">
                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                    <input type="checkbox" class="checkboxes" value="1" />
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td> {{ $key + 1 }} </td>                                       
                                            <td> {{ $payment->payment_type }} </td>                                            
                                            <td style="text-align:center">                                                
                                                <a onclick="event.preventDefault(); editPayment({{ $payment->id }});" 
                                                    title="Edit" data-placement="top" data-toggle="tooltip" data-original-title="Edit" class="btn btn-success btn-xs tooltips">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>

                                                <a href=""
                                                onclick="event.preventDefault(); deletePayment({{ $payment->id }});"
                                                title="Delete Payment" data-placement="top" data-toggle="tooltip" data-original-title="Delete Payment" class="btn btn-danger btn-xs tooltips">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                            <form id="delete-payment-{{ $payment->id }}" action="{{route('admin.payment.destroy',$payment->id)}}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>

<!-- /.modal for new payment -->
<div class="modal fade" id="newPayment" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">New Payment</h4>
            </div>
            <form role="form" action="{{route('admin.payment.store')}}" method="POST">
                @csrf
                <div class="modal-body"> 
                    <div class="form-group">
                        <label>Payment type</label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Payment type" type="text" name="payment_type" required="required"> 
                        </div>
                    </div>                   

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal for new payment -->

<!-- /.modal for edit payment -->
<div class="modal fade" id="editPayment" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Payment</h4>
            </div>
            <form role="form" action="{{route('admin.payment.update',0)}}" method="POST">
                @csrf
                @method('PATCH')
                <div id="idField"></div>
                <div class="modal-body"> 
                    <div class="form-group">
                        <label>Payment type</label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            <div id="typeField"></div>     
                        </div>
                    </div>                   

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal for edit payment -->



@endsection

@push('js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
    function editPayment(id){
        var url = "{{url('/admin/payment/edit')}}/" + id;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            $('#editPayment').modal('show');
            var $typeField = $('#typeField');
            $typeField.empty();
            $typeField.append('<input class="form-control input-circle-right" placeholder="Payment type" type="text" name="payment_type" value="'+ data.payment.payment_type +'"> ');

            var $idField = $('#idField');
            $idField.empty();
            $idField.append('<input class="form-control" type="hidden" name="id" value="'+ data.payment.id +'"> ');
        });
    }





    function deleteVeriant(id){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {                
                document.getElementById('delete-variant-'+id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your file is safe :)',
                'error'
                )
            }
            })
    }
</script>
@endpush
