@extends('layouts.admin')
@section('title', 'Shipper')
@push('css')
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('breathcamp')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{url('/home')}}" >Dashboard</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="{{route('admin.shipper.index')}}">Shipper</a>
    </li>
</ul>
@endsection
@section('content')
        <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a id="sample_editable_1_new" class="btn sbold green" data-toggle="modal" href="#newShipper" > Add New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-print"></i> Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;">
                                                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                    <tr>
                                        <th style="display:none">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                <span></span>
                                            </label>
                                        </th>
                                        <th> Sl. </th>
                                        <th> Company Name </th> 
                                        <th> Phone </th>                                        
                                        <th> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>
                                        @foreach($shippers as $key => $shipper)
                                        <tr class="odd gradeX"> 
                                            <td style="display:none">
                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                    <input type="checkbox" class="checkboxes" value="1" />
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td> {{ $key + 1 }} </td>                                       
                                            <td> {{ $shipper->company_name }} </td> 
                                            <td> {{ $shipper->phone }} </td>                                            
                                            <td style="text-align:center">                                                
                                                <a onclick="event.preventDefault(); editShipper({{ $shipper->id }});" 
                                                    title="Edit" data-placement="top" data-toggle="tooltip" data-original-title="Edit" class="btn btn-success btn-xs tooltips">
                                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                                </a>

                                                <a href=""
                                                onclick="event.preventDefault(); deleteShipper({{ $shipper->id }});"
                                                title="Delete Shipper" data-placement="top" data-toggle="tooltip" data-original-title="Delete Shipper" class="btn btn-danger btn-xs tooltips">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                            <form id="delete-shipper-{{ $shipper->id }}" action="{{route('admin.shipper.destroy',$shipper->id)}}" method="POST" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>

<!-- /.modal for new shipper -->
<div class="modal fade" id="newShipper" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">New Shipper</h4>
            </div>
            <form role="form" action="{{route('admin.shipper.store')}}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                        <label>Company Name <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Company Name" type="text" name="company_name" required="required"> 
                            <span class="text-danger">{{ $errors->first('company_name') }}</span>
                        </div>
                    </div>
                    
                    
                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        <label>Phone <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-check-square-o"></i>
                            </span>
                            <input class="form-control input-circle-right" placeholder="Phone" type="text" name="phone" required="required"> 
                            <span class="text-danger">{{ $errors->first('phone') }}</span>                              
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Save</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal for new shipper -->

<!-- /.modal for edit shipper -->
<div class="modal fade" id="editShipper" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Shipper</h4>
            </div>
            <form role="form" action="{{route('admin.shipper.update',0)}}" method="POST">
                @csrf
                @method('PATCH')
                <div id="idField"></div>
                <div class="modal-body"> 
                    <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                        <label>Company Name <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-tag"></i>
                            </span>
                            <div id="nameField"></div>     
                            <span class="text-danger">{{ $errors->first('company_name') }}</span>
                        </div>
                    </div> 
                    
                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                        <label>Phone <span class="text-danger"> * </span></label>
                        <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-check-square-o"></i>
                            </span>
                            <div id="phoneField"></div>
                            <span class="text-danger">{{ $errors->first('phone') }}</span>                              
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn green">Update</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal for edit variant -->



@endsection

@push('js')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
    function editShipper(id){
        var url = "{{url('/admin/shipper/edit')}}/" + id;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            $('#editShipper').modal('show');
            var $nameField = $('#nameField');
            $nameField.empty();
            $nameField.append('<input class="form-control input-circle-right" placeholder="Company Name" type="text" name="company_name" value="'+ data.shipper.company_name +'"> ');
            
            var $phoneField = $('#phoneField');
            $phoneField.empty();
            $phoneField.append('<input class="form-control input-circle-right" placeholder="Phone" type="text" name="phone" value="'+ data.shipper.phone +'"> ');

            var $idField = $('#idField');
            $idField.empty();
            $idField.append('<input class="form-control" type="hidden" name="id" value="'+ data.shipper.id +'"> ');
        });
    }

    function deleteVeriantOption(id){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {                
                document.getElementById('delete-voption-'+id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your file is safe :)',
                'error'
                )
            }
            })
    }
</script>
@endpush
