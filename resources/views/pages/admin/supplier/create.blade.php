@extends('layouts.admin')
@section('title', 'Supplier')
@push('css')

@endpush
@section('breathcamp')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{url('/home')}}" >Dashboard</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="{{route('admin.supplier.index')}}">Supplier</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="#">New Supplier</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
    <div class="col-md-offset-1 col-md-10">
            <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                             New Supplier </div>
                        <div class="tools">
                            <a href="" class="collapse"> </a>
                            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                            <a href="" class="reload"> </a>
                            <a href="" class="remove"> </a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                    <form role="form" class="form-horizontal" method="POST" action="{{route('admin.supplier.store')}}">
                        @csrf                          
                        <div class="form-body">
                                <div class="form-group has-success {{ $errors->has('company_name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label">Company Name<span class="text-danger"> * </span></label>
                                    <div class="col-md-9">
                                    <input type="text" name="company_name" value="{{ old('company_name') }}" class="form-control"> 
                                    <span class="text-danger">{{ $errors->first('company_name') }}</span>  
                                    </div>
                                </div>
                                <div class="form-group has-success ">
                                    <label class="col-md-3 control-label">Company Address</label>
                                    <div class="col-md-9">                                        
                                        <input type="text" name="company_address" value="{{ old('company_address') }}" class="form-control">                                         
                                    </div>
                                </div>
                                <div class="form-group has-success">
                                        <label class="col-md-3 control-label">City</label>
                                        <div class="col-md-9">
                                        <input type="text" name="company_city" value="{{ old('company_city') }}" class="form-control"> 
                                        </div>
                                    </div>
                                    <div class="form-group has-success {{ $errors->has('company_phone') ? 'has-error' : '' }}">
                                        <label class="col-md-3 control-label">Company Phone<span class="text-danger"> * </span></label>
                                        <div class="col-md-9">                                            
                                            <input type="text" name="company_phone" value="{{ old('company_phone') }}" class="form-control"> 
                                            <span class="text-danger">{{ $errors->first('company_phone') }}</span>  
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                            <label class="col-md-3 control-label">Company Email</label>
                                            <div class="col-md-9">
                                                <input type="text" name="company_email" value="{{ old('company_email') }}" class="form-control"> 
                                            </div>
                                        </div>
                                        <div class="form-group has-success {{ $errors->has('contact_person_name') ? 'has-error' : '' }}">
                                            <label class="col-md-3 control-label">Contact Person<span class="text-danger"> * </span></label>
                                            <div class="col-md-9">                                                
                                                <input type="text" name="contact_person_name" value="{{ old('contact_person_name') }}" class="form-control"> 
                                                <span class="text-danger">{{ $errors->first('cotact_person_name') }}</span>  
                                            </div>
                                        </div>
                                        <div class="form-group has-success {{ $errors->has('contact_person_phone') ? 'has-error' : '' }}">
                                                <label class="col-md-3 control-label">Contact Person Phone<span class="text-danger"> * </span></label>
                                                <div class="col-md-9">
                                                    <input type="text" name="contact_person_phone" value="{{ old('contact_person_phone') }}" class="form-control"> 
                                                    <span class="text-danger">{{ $errors->first('contact_person_phone') }}</span>  
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-md-3 control-label">Contact Person Email</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="contact_person_email" value="{{ old('contact_person_email') }}" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-md-3 control-label">Payment Status</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="payment_status" value="{{ old('payment_status') }}" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-md-3 control-label">Payment Method</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="payment_methods" value="{{ old('payment_methods')}}" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-md-3 control-label">Discount (%)</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="discount" value="{{ old('discount') }}" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                <label class="col-md-3 control-label">Total Amount</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="total_amount" value="{{ old('total_amount') }}" class="form-control"> 
                                                </div>
                                            </div>
                                            <div class="form-group has-success">
                                                    <label class="col-md-3 control-label">Type of Goods</label>
                                                    <div class="col-md-9">
                                                        <input type="text" name="type_of_goods" value="{{ old('type_of_goods') }}" class="form-control"> 
                                                    </div>
                                                </div>
                                                <div class="form-group has-success">
                                                    <label class="col-md-3 control-label">Goods Short Description</label>
                                                    <div class="col-md-9">
                                                        <input type="text" name="goods_description" value="{{ old('goods_discription') }}" class="form-control"> 
                                                    </div>
                                                </div>
                                
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <a href="{{route('admin.supplier.index')}}" class="btn default">Cancel</a>
                                        <button type="submit" name="submit" class="btn blue">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
    </div>
</div>
@endsection

@push('js')

@endpush