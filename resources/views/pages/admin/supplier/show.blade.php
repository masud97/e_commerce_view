@extends('layouts.admin')
@section('title', 'Supplier')
@push('css')

@endpush
@section('breathcamp')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{url('/home')}}" >Dashboard</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="{{route('admin.supplier.index')}}">Supplier</a>
    </li>
    <li class="active">
        <i class="fa fa-circle"></i>
        <a href="#">Details</a>
    </li>
</ul>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        Supplier Details
                    </div>
                    <div class="tools">
                        <a href="{{route('admin.supplier.index')}}" title="Supplier List" data-placement="top" data-toggle="tooltip" data-original-title="Supplier List" class="btn btn-primary btn-xs tooltips" style="height:20px">
                                <i class="fa fa-list" aria-hidden="true"></i>
                            </a>

                            <a href="{{route('admin.supplier.edit',$supplier->id)}}" title="Edit" data-placement="top" data-toggle="tooltip" data-original-title="Edit" class="btn btn-primary btn-xs tooltips" style="height:20px">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>

                            <a href=""
                            onclick="event.preventDefault(); deleteSupplier({{ $supplier->id }});"
                            title="Delete Supplier" data-placement="top" data-toggle="tooltip" data-original-title="Delete Supplier" class="btn btn-danger btn-xs tooltips" style="height:20px">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                        <form id="delete-supplier-{{ $supplier->id }}" action="{{route('admin.supplier.destroy',$supplier->id)}}" method="POST" style="display: none;">
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>
                </div>
                <div class="portlet-body flip-scroll">
                    <table class="table table-bordered table-striped table-condensed flip-content table-striped">
                        <tbody>
                            <tr>
                                <th> Company Name </th>
                                <td> {{ $supplier->company_name }} </td>
                                <th> Company Address </th>
                                <td> {{ $supplier->company_address }} </td>                                
                            </tr>
                            <tr>
                                <th> City </th>
                                <td> {{ $supplier->company_city }} </td>
                                <th> Company phone </th>
                                <td> {{ $supplier->company_phone }} </td>                                
                            </tr>

                            <tr>
                                <th> Company Email </th>
                                <td> {{ $supplier->company_email }} </td>
                                <th> Contact Person </th>
                                <td> {{ $supplier->contact_person_name }} </td>
                            </tr>

                            <tr>
                                <th> Person phone </th>
                                <td> {{ $supplier->contact_person_phone }} </td>
                                <th> Person Email </th>
                                <td> {{ $supplier->contact_person_email }} </td>                                
                            </tr>

                            <tr>
                                <th> Payment Status </th>
                                <td> 
                                    @if($supplier->payment_status == 'Paid')
                                    <span class="label label-sm label-success">{{ $supplier->payment_status }}</span> 
                                    @else
                                    <span class="label label-sm label-warning">{{ $supplier->payment_status }}</span>
                                    @endif
                                </td>
                                <th> Payment Method </th>
                                <td> 
                                    @if($supplier->payment_methods)
                                    <span class="label label-sm label-success"> {{ $supplier->payment_methods }} </span>
                                    @else
                                    <span class="label label-sm label-warning"> Nonpayment </span>
                                    @endif
                                </td>                                
                            </tr>

                            <tr>
                                <th> Discount </th>
                                <td> <span class="label label-sm label-primary"> {{ $supplier->discount }} &#37; </label> </td>
                                <th> Total Amount </th>
                                <td> <span class="label label-sm label-primary"> {{ $supplier->total_amount }} </label> </td>
                            </tr>

                            <tr>
                                <th> Goods Type </th>
                                <td> {{ $supplier->type_of_goods }} </td>
                                <th> Goods Description </th>
                                <td> {{ $supplier->goods_description }} </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection

@push('js')
<script type="text/javascript">
    function deleteSupplier(id){
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {                
                document.getElementById('delete-supplier-'+id).submit();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your file is safe :)',
                'error'
                )
            }
            })
    }
</script>
@endpush