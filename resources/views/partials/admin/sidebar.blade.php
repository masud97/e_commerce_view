<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- BEGIN SIDEBAR -->
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
			<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			<li class="sidebar-toggler-wrapper hide">
				<div class="sidebar-toggler">
					<span></span>
				</div>
			</li>
			<!-- END SIDEBAR TOGGLER BUTTON -->
			<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
			<li class="sidebar-search-wrapper">
				<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
				<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
				<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<form class="sidebar-search  sidebar-search-bordered" action="page_general_search_3.html" method="POST">
					<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
					</a>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<a href="javascript:;" class="btn submit">
								<i class="icon-magnifier"></i>
							</a>
						</span>
					</div>
				</form>
				<!-- END RESPONSIVE QUICK SEARCH FORM -->
			</li>
			<li class="nav-item start {{ Request::is('/home') ? 'active' : ''}}">
				<a href="{{url('/home')}}" class="nav-link nav-toggle">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>                               
				</a>
			</li>
			
			<li class="nav-item start {{ Request::is('admin/category*') ? 'active' : ''}}">
				<a href="{{route('admin.category.index')}}" class="nav-link nav-toggle">
					<i class="icon-list"></i>
					<span class="title">Categories</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/customer*') ? 'active' : ''}}">
				<a href="{{route('admin.customer.index')}}" class="nav-link nav-toggle">
					<i class="icon-users"></i>
					<span class="title">Customers</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/payment*') ? 'active' : ''}}">
				<a href="{{route('admin.payment.index')}}" class="nav-link nav-toggle">
					<i class="icon-wallet"></i>
					<span class="title">Payments</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/product*') ? 'active' : ''}}">
				<a href="{{route('admin.product.index')}}" class="nav-link nav-toggle">
					<i class="icon-present"></i>
					<span class="title">Products</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/supplier*') ? 'active' : ''}}">
				<a href="{{route('admin.supplier.index')}}" class="nav-link nav-toggle">
					<i class="icon-layers"></i>
					<span class="title">Suppliers</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/shipper*') ? 'active' : ''}}">
				<a href="{{route('admin.shipper.index')}}" class="nav-link nav-toggle">
					<i class="icon-bag"></i>
					<span class="title">Shippers</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/variant*') ? 'active' : ''}}">
				<a href="{{route('admin.variant.index')}}" class="nav-link nav-toggle">
					<i class="icon-tag"></i>
					<span class="title">Variants</span>                               
				</a>
			</li>

			<li class="nav-item start {{ Request::is('admin/voption*') ? 'active' : ''}}">
				<a href="{{route('admin.voption.index')}}" class="nav-link nav-toggle">
					<i class="icon-support"></i>
					<span class="title">Variant Options</span>                               
				</a>
			</li>

			<li class="heading">
				<h3 class="uppercase">Features</h3>
			</li>
			
			<li class="nav-item start ">
				<a href="javascript:;" class="nav-link nav-toggle">
					<i class="icon-social-dribbble"></i>
					<span class="title">General</span>
					<span class="arrow"></span>
				</a>
				<ul class="sub-menu">
					<li class="nav-item  ">
						<a href="page_general_about.html" class="nav-link ">
							<i class="icon-info"></i>
							<span class="title">About</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="page_general_contact.html" class="nav-link ">
							<i class="icon-call-end"></i>
							<span class="title">Contact</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-notebook"></i>
							<span class="title">Portfolio</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item ">
								<a href="page_general_portfolio_1.html" class="nav-link "> Portfolio 1 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_portfolio_2.html" class="nav-link "> Portfolio 2 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_portfolio_3.html" class="nav-link "> Portfolio 3 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_portfolio_4.html" class="nav-link "> Portfolio 4 </a>
							</li>
						</ul>
					</li>
					<li class="nav-item  ">
						<a href="javascript:;" class="nav-link nav-toggle">
							<i class="icon-magnifier"></i>
							<span class="title">Search</span>
							<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li class="nav-item ">
								<a href="page_general_search.html" class="nav-link "> Search 1 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_search_2.html" class="nav-link "> Search 2 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_search_3.html" class="nav-link "> Search 3 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_search_4.html" class="nav-link "> Search 4 </a>
							</li>
							<li class="nav-item ">
								<a href="page_general_search_5.html" class="nav-link "> Search 5 </a>
							</li>
						</ul>
					</li>
					<li class="nav-item  ">
						<a href="page_general_pricing.html" class="nav-link ">
							<i class="icon-tag"></i>
							<span class="title">Pricing</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="page_general_faq.html" class="nav-link ">
							<i class="icon-wrench"></i>
							<span class="title">FAQ</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="page_general_blog.html" class="nav-link ">
							<i class="icon-pencil"></i>
							<span class="title">Blog</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="page_general_blog_post.html" class="nav-link ">
							<i class="icon-note"></i>
							<span class="title">Blog Post</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="page_general_invoice.html" class="nav-link ">
							<i class="icon-envelope"></i>
							<span class="title">Invoice</span>
						</a>
					</li>
					<li class="nav-item  ">
						<a href="page_general_invoice_2.html" class="nav-link ">
							<i class="icon-envelope"></i>
							<span class="title">Invoice 2</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
		<!-- END SIDEBAR MENU -->
	</div>
	<!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->